package com.devcamp.drinklistjpa.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinklistjpa.model.Drink;
import com.devcamp.drinklistjpa.repository.DrinkRepository;

@RestController("/")
@CrossOrigin
@RequestMapping
public class DrinkController {
    @Autowired
    DrinkRepository drinkRespo;
   

    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrinks(){
        try {
            List<Drink> listCustomer = new ArrayList<Drink>();
            
            drinkRespo.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);


        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
