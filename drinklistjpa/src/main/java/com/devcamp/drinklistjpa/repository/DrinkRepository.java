package com.devcamp.drinklistjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drinklistjpa.model.Drink;
public interface DrinkRepository extends JpaRepository<Drink, Long>{
    
}
